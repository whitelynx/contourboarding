Driving LEDs from I/O pins
==========================

Duplexed LEDs using 3-state I/O pins
------------------------------------
Microcontroller I/O pins can be set to one of 3 states: high, low, or high-impedance (NC); therefore, we can drive 2
LEDs off each pin:

                      +5V
                       ╤
                       │
    ═╗           ⇗     ▮ R1
     ║       ┌──┤◀──┐  │
    P║       │ LED1 │  │
    I╟───────┤      ├──┤   |R1| == |R2|
    C║PIN1   │   ⇗  │  │
     ║       └──▶├──┘  │
    ═╝         LED2    ▮ R2
                       │
                       ╧
                      GND

When **PIN1** is in the high-impedance state, neither LED is lit. When **PIN1** is low, **LED1** will be on; when
**PIN1** is high, **LED2** will be lit. You can turn both on by alternating quickly (~35 Hz or more) between the high
and low states.

The rate at which you would need to switch states may be reduced by adding one (non-LED) diode between each LED and
the microcontroller, and adding a capacitor in parallel with each LED. The exact values of these capacitors are left
as an exercise for the reader.

This design has the advantage of a very low required pin count, needing only `n/2` pins for `n` LEDs. It does, however,
have some disadvantages, such as the constant current drain from **+5V** to **GND** through **R1** and **R2**. If the
resistors are of a sufficiently high value, the effect may be negligible, but the constant power drain may be a
consideration in low-power projects. Also, this requires timing logic to switch each pin on and off at a sufficient
rate if both of the LEDs are lit.


Addressable gated D latch
-------------------------
As an alternative to using 3-state pins to drive LEDs directly, an external addressable latch IC (preferably with gated
D latches or D flip-flops) could be used to hold the current LED state and drive the connected LEDs:

    ═╗               ╔═══════╗   ⇗
     ║  address[0:2] ║       ╟──▶├──┐
     ╟───────────────╢       ║ LED1 │
     ╟───────────────╢       ║      │
     ╟───────────────╢   D   ║   ⇗  │
     ║               ║       ╟──▶├──┤
    P║               ║   L   ║ LED2 │
    I║               ║   A   ║      │
    C║          data ║   T   ║  ... │
     ╟───────────────╢   C   ║      │
     ║               ║   H   ║   ⇗  │
     ║        enable ║       ╟──▶├──┤
     ╟───────────────╢       ║ LED8 │
     ║               ║       ║      │
    ═╝               ╚═══════╝      ▮ R1
                                    │
                                    ╧
                                   GND

This design has a higher required pin count than the above, requiring `2 + log2(n)` pins for `n` LEDs (it requires
enable, data, and `log2(n)` address pins) but can scale up to more more LEDs, similar to a demuxer. Also, this has the
advantage of allowing you to reuse the address and data pins for other functions, provided each function has its own
enable pin. It does, however, require you to change addresses every time you need to change an LED, so it does not
allow bulk state loading like the shift register does. (even the duplexed 3-state design above could allow for bulk
state updates if coded in a certain way) This may not be an issue for LEDs, though.


Shift register
--------------
Another alternative (which is probably the simplest and most time-efficient) would be to use a shift register to
store the desired states of all LEDs:

    ═╗               ╔═══════╗   ⇗
     ║     data[0:7] ║       ╟──▶├──┐
     ╟───────────────╢       ║ LED1 │
     ╟───────────────╢   R   ║      │
     ╟───────────────╢   E   ║   ⇗  │
     ╟───────────────╢   G   ╟──▶├──┤
    P╟───────────────╢   I   ║ LED2 │
    I╟───────────────╢   S   ║      │
    C╟───────────────╢   T   ║  ... │
     ╟───────────────╢   E   ║      │
     ║               ║   R   ║   ⇗  │
     ║           set ║       ╟──▶├──┤
     ╟───────────────╢       ║ LED8 │
     ║               ║       ║      │
    ═╝               ╚═══════╝      ▮ R1
                                    │
                                    ╧
                                   GND

This has similar advantages to the gated D latch above: all data pins can be reused for other functions, and there is
no constant current drain except through lit LEDs. However, it has the highest required pin count of the 3 designs,
requiring `n+1` pins for `n` LEDs. Note that this is still a bit better for pin count than just driving each LED
directly, because the **data** pins can all be reused for other functions; they are only used for setting LED state
when the **set** pin is driven high.

Also, using multiple shift registers (possibly with lower numbers of data lines) would allow us to multiplex, reducing
pin count slightly; for example, using two 4-line shift registers would allow us to drive 8 LEDs using a total of 6
pins, 4 of which (the data lines) are still reusable for other purposes.
