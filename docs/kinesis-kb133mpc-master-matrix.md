NOTE: Key names are listed in the `usb_hid` module.

Note: All layouts below are for the 100-key variant, not the base 86-key. I want more keys, dammit.

**TODO: Determine fixed sub-matrices for ESC, F1-F12, etc! Also, update key matrices for these keys!**


Fixed sub-matrices
==================

These sub-matrices are dictated by the original keyboard's wiring, and can't be changed without replacing the entire
existing matrix wiring. They can, however, be transposed. (see the 20 x 5 layouts)


Left-side key well (=12345)
---------------------------

                                     Inputs (+)
                    1       2       3       4       5       6
                5   =       1       2       3       4       5
                4   Tab     Q       W       E       R       T
    Outputs (-) 3   CapsLck A       S       D       F       G
                2   LShift  Z       X       C       V       B
                1   ___     `       INS  Left    ???     Right?


Left-side thumb keys
--------------------
No, the "rows" here really don't make any sense, especially since 2 of them could be combined.

                    Inputs (+)
                    1       2
                4   LCtrl   LAlt
    Outputs (-) 3   Bksp    Home
                2   Delete  ___
                1   ___     End


Right-side key well (67890-)
----------------------------

                                     Inputs (+)
                    1       2       3       4       5       6
                5   6       7       8       9       0       -
                4   Y       U       I       O       P       \
    Outputs (-) 3   H       J       K       L       ;       '
                2   N       M       ,       .       /       RShift
                1   Up?     ???     Down    [       ]       ___


Right-side thumb keys
---------------------
No, the "rows" here really don't make any sense, especially since 2 of them could be combined.

                    Inputs (+)
                    1       2
                4   RAlt    ___
    Outputs (-) 3   PgUp    RCtrl
                2   ___     Enter
                1   PgDown  Space


Key Matrices
============

Logic levels:

- **LOW:** 0 V (ground)
- **HIGH:** +5 V

In the matrices below, unless otherwise specified:

- The horizontal axis corresponds to matrix input lines (microcontroller -> matrix) and the vertical axis corresponds
  to matrix output lines. (matrix -> microcontroller)
- Matrix inputs must be normally held LOW and then held HIGH in order to read that column's key presses.
- Matrix outputs will normally be LOW, but will change to HIGH to indicate a key press at the given position.


Split Key Matrices
------------------

Since I've added the requirement of having the keyboard split in two pieces, I'll actually want to implement two
separate matrices: one for each side of the keyboard. Each matrix will have its own microcontroller dedicated to
scanning and reporting keypresses, and they will both talk to the central microcontroller (which will handle USB HID
and input from the TrackPoint as well) using I2C.


### 6 x 9 per side
- **Input lines:** 6 (4 with 1-to-8 demux)
- **Output lines:** 9 (1 1/8 bytes)
- **Total keys:** 108 (54 * 2)

See [contourboarding_6x9_left.keymatrix](../keymatrices/contourboarding_6x9_left.keymatrix) and
[contourboarding_6x9_right.keymatrix](../keymatrices/contourboarding_6x9_right.keymatrix) for layouts.

This is the optimal configuration for response time (scan speed); it has the lowest number of inputs possible while
still accomodating the fixed sub-matrices.


### 7 x 8 per side
- **Input lines:** 7 (4 with 1-to-8 demux)
- **Output lines:** 8 (1 byte)
- **Total keys:** 112 (56 * 2)

This layout sadly still cannot accomodate the original fixed sub-matrices unmodified. The main key wells will work
fine, but the thumb clusters would need to be reworked in order to use this matrix configuration.


### 8 x 7 per side
- **Input lines:** 8 (4 with 1-to-8 demux)
- **Output lines:** 7 (7/8 bytes)
- **Total keys:** 112 (56 * 2)

See [contourboarding_8x7_left.keymatrix](../keymatrices/contourboarding_8x7_left.keymatrix) and
[contourboarding_8x7_right.keymatrix](../keymatrices/contourboarding_8x7_right.keymatrix) for layouts.

This is the optimal configuration for number of pins; it gives us the most possible keys, with the least used pins.


----


Old Merged Layouts
------------------


### 6 x 17
- **Input lines:** 6 (4 with 1-to-8 demux)
- **Output lines:** 17 (2 1/8 bytes)
- **Total keys:** 102

See [contourboarding_6x17.keymatrix](../keymatrices/contourboarding_6x17.keymatrix) for layout.


### 7 x 15
- **Input lines:** 7 (4 with 1-to-8 demux)
- **Output lines:** 15 (1 7/8 bytes)
- **Total keys:** 105


### 8 x 13
- **Input lines:** 8 (4 with 1-to-8 demux)
- **Output lines:** 13 (1 5/8 bytes)
- **Total keys:** 104


### 20 x 5 with Inverted Matrices

This design requires the following:

- The microcontroller should drive the inputs (which would normally be "output" lines) using a bank of inverters on
  the matrix side of the demux. (normally high, pulled low when checking a given input)
- The outputs (normally "input" lines) should be pulled high, and should be read as inverted inputs by the
  microcontroller.

These designs have the advantage of being slightly faster since they require fewer input pins, but the inversion is
necessary in order to accomodate the 6 x 5 key well sub-matrices.

- **Input lines:** 5 (4 with 1-to-8 demux)
- **Output lines:** 20 (2 1/2 bytes)
- **Total keys:** 100
- **Layout options:** 5

See layouts:
- [contourboarding_20x5_v1.keymatrix](../keymatrices/contourboarding_20x5_v1.keymatrix)
- [contourboarding_20x5_v2.keymatrix](../keymatrices/contourboarding_20x5_v2.keymatrix)
- [contourboarding_20x5_v3.keymatrix](../keymatrices/contourboarding_20x5_v3.keymatrix)
- [contourboarding_20x5_v4.keymatrix](../keymatrices/contourboarding_20x5_v4.keymatrix)
- [contourboarding_20x5_v5.keymatrix](../keymatrices/contourboarding_20x5_v5.keymatrix)
