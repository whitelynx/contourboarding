# IBM TrackPoint module

## Pinout
Pins listed from bottom to top, if you hold the module so that you can read the letters printed on the back. ("R32" and
"4A")

1. `RST` (has some component tying it to ground, though it doesn't show as having any connection with an ohmmeter... do
  we need the 2.2uF/100K RC network from the datasheet on this pin?)
2. `RXD/T0` (PS/2 from secondary mouse: `DATA`)
3. `TXD/T1` (PS/2 to host: `DATA`)
4. `INT1` (PS/2 from secondary mouse: `CLK`)
5. `VSS` (`GND`)
6. `VCC` (`+5V`)
7. `INT0` (PS/2 to host: `CLK`)
8. `P3.0` (left button)
9. `P3.1` (right button)
10. `P3.2` (middle button)

The TrackPoint chip uses the PS/2 protocol to communicate with the host circuitry, which according to Wikipedia is a
serial protocol at 10 to 16 kHz with 1 stop bit, 1 start bit, and 1 parity bit (odd).

The parity bit must be implemented in software, since the PIC's EUSART doesn't have hardware parity. It is stored as
the 9th data bit, and should be set such that the total number of high bits in the character (including the parity bit
itself) is odd.
