# Kinesis Contoured Keyboard USB-enabled Core: Software

## Requirements
- provide a USB HID-compliant keyboard interface
    - also provide a boot interface descriptor (see [USB HID 1.11 spec][], Appendix B)
- switchable user-defined keymaps
    - Allow keymaps to be managed directly through a [USB mass storage][] endpoint on the keyboard? (either that, or it
        will require special software in order to manage keymaps)
- fast response times - allow for fast typists without keyboard getting "confused" like the stock Kinesis firmware does

[USB HID 1.11 spec]: http://www.usb.org/developers/devclass_docs/HID1_11.pdf
[USB mass storage]: http://www.usb.org/developers/devclass_docs/usb_msc_cbi_1.1.pdf


## High-Level Design

### State variables
- `last matrix state` _(bit array)_: stores the state of the input pins for the last full key matrix scan, indexed the
    same way as the keymap
- `send event` _(boolean)_: whether or not the state has changed since `last matrix state` was last updated; if set to
    `TRUE`, we need to send a USB event with the new key state
- `pressed keys` _(byte array)_: stores the USB scan codes of all currently pressed keys
- `outstanding mouse event` _(**FIXME: Unknown type; some sort of struct?**)_: stores event data for any mouse event
    that has happened since the previous scan finished

### Main logic flow
- set `last matrix state` to all zeros
- main loop
    - set `send event` to False
    - clear `pressed keys`
    - for each query line:
        - set up output pins to select the query line
        - wait for propogation delay
        - read all response lines
        - for each response line:
            - if response line doesn't match the corresponding bit in `last matrix state`:
                - set `send event` to True
            - if response line is `1`:
                - look up scan code in current keymap (use `(query line number, response line number)` as coordinates)
                - append scan code to `pressed keys` (the function to do this should be written to send phantom
                    keypress events if more pressed keys are detected than the max)
    - if `send event` is True:
        - send USB event for `pressed keys`
    - if `outstanding mouse event` is not empty:
        - send USB event for `mouse move` or `mouse button press`, as appropriate
        - clear `outstanding mouse event`

### Interrupts
- EUSART synchronous receive (RCIF)
    - translate data to mouse move/button event data
    - store mouse event data in `outstanding mouse event`
