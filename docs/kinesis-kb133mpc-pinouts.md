Rows are numbered from the bottom up, columns from left to right as you are looking at the top of the keyboard.  
Left, right, top, and bottom are relative to the top of the keyboard. (i.e., it matches what it would look like if the
keyboard were assembled and you were typing on it)


There's a lot of resistance on each line when a key is pressed; my multimeter eventually settles somewhere between 550
kΩ and 800 kΩ, but it takes a while to drop to that point when the key is pressed. I'll have to check the output
voltage with a +5 V supply to see how it'll look to the microcontroller.


Pins marked with a `+` are matrix input pins; these require a positive voltage to be applied to them because of the
isolation diodes. These can be driven off the microcontroller pins directly, or off a demultiplexer.  
Pins marked with a `-` are matrix output pins; these must be floating outputs because of the isolation diodes. These
must be read directly with microcontroller pins.

Because the thumb wells each have 2 rows that are missing a key, the pins for those 2 rows could be tied together,
saving a couple of pins on the demux.


Common Matrix Legend:
---------------------
```
___   unused
???   unused, but makes no sense
Key?  position makes no sense
```


Left-side key well (=12345):
----------------------------
```
 1 + row 1 (bottom)
 2 + row 2
 3 + row 3
 4   GND
 5 + row 4
 6 - col 1 (leftmost)
 7 - col 2
 8 - col 3
 9   GND
10 - col 4
11 - col 5
12 - col 6 (rightmost)
13 + row 5 (top)
```

### Matrix:
```
                                 Inputs (+)
                1       2       3       4       5       6
            5   =       1       2       3       4       5
            4   Tab     Q       W       E       R       T
Outputs (-) 3   CapsLck A       S       D       F       G
            2   LShift  Z       X       C       V       B
            1   ___     `       Insert  Left    ???     Right?
```

Mismatched key positions:
 - right arrow - row 1, col 6 (physically positioned at row 1, col 5)

Unused key positions:
 - row 1, col 1
 - row 1, col 5


Left-side thumb keys:
---------------------
(no, the "rows" really don't make any sense here, especially since 2 of them could be combined)

```
 1 - row 2 (Delete)
 2 - row 1 (End)
 3 - row 4 (Ctrl and Alt)
 4 + col 1 (Ctrl, Delete, and Backspace)
 5 + col 2 (Alt, Home, and End)
 6   ...?
 7   that weird little trace around the edge of the board
 8 - row 3 (Backspace and Home)
 9   GND
10   ...?
```

### Matrix:
```
                Inputs (+)
                1       2
            4   LCtrl   LAlt
Outputs (-) 3   Bksp    Home
            2   Delete  ___
            1   ___     End
```


Right-side key well (67890-):
-----------------------------
```
 1 - col 1 (leftmost)
 2 - col 3
 3 - col 2
 4 + row 1 (bottom)
 5   GND
 6 - col 6 (rightmost)
 7 + row 2
 8 + row 3
 9 + row 4
10 + row 5 (top)
11   GND
12 - col 4
13 - col 5
```

### Matrix:
```
                                 Inputs (+)
                1       2       3       4       5       6
            5   6       7       8       9       0       -
            4   Y       U       I       O       P       \
Outputs (-) 3   H       J       K       L       ;       '
            2   N       M       ,       .       /       RShift
            1   Up?     ???     Down    [       ]       ___
```

Mismatched key positions:
 - up arrow - row 1, col 1 (physically positioned at row 1, col 2)

Unused key positions:
 - row 1, col 2
 - row 1, col 6


Right-side thumb keys:
----------------------
(no, the "rows" really don't make any sense here, especially since 2 of them could be combined)

```
 1   ...?
 2   that weird little trace around the edge of the board
 3 - row 1 (Page Down and Space)
 4 - row 2 (Enter)
 5 - row 3 (Page Up and Ctrl)
 6 - row 4 (Alt)
 7 + col 1 (Alt, Page Up, and Page Down)
 8 + col 2 (Ctrl, Enter, and Space)
 9   GND
10   ...?
```

### Matrix:
```
                Inputs (+)
                1       2
            4   RAlt    ___
Outputs (-) 3   PgUp    RCtrl
            2   ___     Enter
            1   PgDown  Space
```
