# Kinesis Contoured Keyboard USB-enabled Core: Hardware

## Key Count
- **Original**
    - Key wells: 2; `56 keys`
        - Keys per well: `28 keys`
            - 2 rows of 4 = `8 keys`
            - 4 rows of 5 = `20 keys`
    - Thumb key groups: 2; `12 keys`
        - Keys per group: `6 keys`
    - Top line of keys: `18 keys`
        - Function keys: `12 keys`
        - Special keys: `6 keys`
            - Esc
            - PrintScr/SysReq
            - Scroll lock
            - Pause/Break
            - Keypad
            - Program
    - **Original Total:** 56 + 12 + 18 = `86 keys`

- **Possible Additional Keys**
    - Extra Meta (Windows) keys (next to Space and Backspace): `2 keys`
    - "Media keys": `12 keys`
        - Transport: `4 keys`
            - Scan Previous Track (`PVT`)
            - Play/Pause (`PLP`)
            - Stop (`STO`)
            - Scan Next Track (`NXT`)
        - Volume: `3 keys`
            - Keyboard Volume Up (`VU`)
            - Keyboard Volume Down (`VD`)
            - Keyboard Mute (`MUT`)
        - Generic hotkeys or additional function keys: `5 keys` (`F13` - `F17`)
    - **Total Additional Keys:** `14 keys`

- **Total With Additional Keys:** 86 + 14 = `100 keys`


## I/O Pins
Total pins on the PIC18F4550: **40 pins**

- **Used Pins:**
    - Fixed: **10 pins**
        - Power (`VDD` x2, `VSS` x2): **4 pins**
        - `MCLR` (for high-voltage programming): **1 pin** (it _might_ be possible to use this as a general I/O pin as
            well, but this would require a hardware switch to disconnect the rest of the circuit when programming)
        - USB: **3 pins**
            - Power (`VUSB`): **1 pin**
            - Data (`D+`, `D-`): **2 pins**
        - Crystal Oscillator: **2 pins**
    - LEDs: **1 pin** (plus **4 shared pins**) using the shift register-based design
        - Caps Lock
        - Num Lock
        - Scroll Lock
        - Keypad Layer
    - TrackPoint (TPM754): **2 pins** (using the PIC's built-in EUSART functionality; uses PS/2 protocol)
        - `TxD` (`RC7`/`RX`/`DT`)
        - `INT0` (`RC6`/`TX`/`CK`? Pretty sure this just needs a clock signal, which just means using synchronous mode
            on the EUSART.)

    - **Total Used:** **13 pins** (plus **4 shared pins**, or more if we want more LEDs)
    - **Remaining:** **27 pins**


## Response Time
- Propagation delays:
    - DM74LS138 _(3- to 8-line decoder / demultiplexer)_: **21 ns**


## Miscellaneous Specs
- Oscillator input rate: **20 MHz**
- Clock rate: ?


## Using Multiple Microcontrollers
As a way to expand the pin count available for polling the key matrices and increase parallelism, one or more outboard
microcontrollers could be used for key matrix polling, sending events to the master microcontroller via I2C or SPI.
This would also free up more pins on the master microcontroller for general I/O, possibly allowing us to integrate
other peripherals.

Using SPI for communication between the master microcontroller and the outboard microcontrollers has the advantage of
also doubling as a programming interface; all we have to do is route the `RESET` line as well, and the master can
upgrade the firmware in the outboard controllers.

The master should be something based on the ATmega32U4 (possibly the [Arduino Micro][] or [SparkFun Pro Micro][]),
since that chip allows us to directly implement USB HID devices. The outboard controllers can be anything with enough
pins; the [Arduino Pro Mini][] would be a good choice.

Another option would be to have the two halves of the keyboard actually be separate USB devices that each plug into the
PC independently. The downside to this is that they can no longer share keymap or configuration information. If we
wanted to take this approach, though, each side could use the [Arduino Micro][].


## Split layouts
Since I've added the requirement of splitting the two halves of the keyboard, we'll actually need 2 separate key
matrices: one for each side. This also simplifies things a bit for building those matrices since each will have its own
dedicated scanning microcontroller that talks to the central microcontroller via SPI.

### Possible half-board keyboard matrix configurations to accomodate 50 keys per side:
- 2 x 25 = 50 (0 extra)   27 total pins (2 output, 25 input)
- 3 x 17 = 51 (1 extra)   20 total pins (3 output, 17 input)
- 4 x 13 = 52 (2 extra)   17 total pins (4 output, 13 input)
- 4 x 13 = 52 (2 extra)   16 total pins (1 output, 2 select, 13 input; using 4 outputs of a 1-to-4 demultiplexer)
- 5 x 10 = 50 (0 extra)   14 total pins (1 output, 3 select, 10 input; using 5 outputs of a 1-to-8 demultiplexer)
- **6 x 9 = 54 (4 extra)   13 total pins (1 output, 3 select, 9 input; using 6 outputs of a 1-to-8 demultiplexer)**
- **7 x 8 = 56 (6 extra)   12 total pins (1 output, 3 select, 8 input; using 7 outputs of a 1-to-8 demultiplexer)**
- **8 x 7 = 56 (6 extra)   11 total pins (1 output, 3 select, 7 input; using 8 outputs of a 1-to-8 demultiplexer)**

Layouts with less than 6 inputs or less than 5 outputs will not accomodate the existing fixed sub-matrices in the
Kinesis. (see [kinesis-kb133mpc-master-matrix.md](./kinesis-kb133mpc-master-matrix.md)) Using layouts like that would
require completely rewiring the key wells.

If we use the [Arduino Pro Mini][] for the outboard microcontrollers, we can support up to 13 digital pins in addition
to SPI. This limits us to the bold layouts above.

The optimal split matrix configuration for number of pins seems to be the 8 x 7 one; see
[contourboarding_8x7_left.keymatrix](../keymatrices/contourboarding_8x7_left.keymatrix) and
[contourboarding_8x7_right.keymatrix](../keymatrices/contourboarding_8x7_right.keymatrix) for layouts.

On the other hand, for speed, we would want the fewest output lines from the microcontroller; this would indicate that
the 6 x 9 matrices would be better for speed of detection. See
[contourboarding_6x9_left.keymatrix](../keymatrices/contourboarding_6x9_left.keymatrix) and
[contourboarding_6x9_right.keymatrix](../keymatrices/contourboarding_6x9_right.keymatrix) for layouts.

----


## Old Merged Layouts
**Outdated:** These layouts were built assuming a single microcontroller would be directly polling all keys. This has
since changed.

To calculate possible key matrix layouts, see calcMatrixLayouts.py.

The bold layouts below are ones I prefer, and am considering.

### Possible keyboard matrix configurations to accomodate 86 keys:
- 2 x 43 = 86 (0 extra)   45 total pins (2 output, 43 input)
- 3 x 29 = 87 (1 extra)   32 total pins (3 output, 29 input)
- **4 x 22 = 88 (2 extra)   25 total pins (1 output, 2 select, 22 input; using 4 outputs of a 1-to-4 demultiplexer)**
- **5 x 18 = 90 (4 extra)   22 total pins (1 output, 3 select, 18 input; using 5 outputs of a 1-to-8 demultiplexer)**
- **6 x 15 = 90 (4 extra)   19 total pins (1 output, 3 select, 15 input; using 6 outputs of a 1-to-8 demultiplexer)**
- **7 x 13 = 91 (5 extra)   17 total pins (1 output, 3 select, 13 input; using 7 outputs of a 1-to-8 demultiplexer)**
- **8 x 11 = 88 (2 extra)   15 total pins (1 output, 3 select, 11 input; using 8 outputs of a 1-to-8 demultiplexer)**
- 9 x 10 = 90 (4 extra)   15 total pins (1 output, 4 select, 10 input; using 9 outputs of a 1-to-16 demultiplexer)

### Possible keyboard matrix configurations to accomodate 100 keys:
- 2 x 50 = 100 (0 extra)   52 total pins (2 output, 50 input)
- 3 x 34 = 102 (2 extra)   37 total pins (3 output, 34 input)
- 4 x 25 = 100 (0 extra)   28 total pins (1 output, 2 select, 25 input; using 4 outputs of a 1-to-4 demultiplexer)
- **5 x 20 = 100 (0 extra)   24 total pins (1 output, 3 select, 20 input; using 5 outputs of a 1-to-8 demultiplexer)**
- **6 x 17 = 102 (2 extra)   21 total pins (1 output, 3 select, 17 input; using 6 outputs of a 1-to-8 demultiplexer)**
- **7 x 15 = 105 (5 extra)   19 total pins (1 output, 3 select, 15 input; using 7 outputs of a 1-to-8 demultiplexer)**
- **8 x 13 = 104 (4 extra)   17 total pins (1 output, 3 select, 13 input; using 8 outputs of a 1-to-8 demultiplexer)**
- 9 x 12 = 108 (8 extra)   17 total pins (1 output, 4 select, 12 input; using 9 outputs of a 1-to-16 demultiplexer)


[Arduino Micro]: https://store.arduino.cc/usa/arduino-micro
[Arduino Pro Mini]: https://store.arduino.cc/usa/arduino-pro-mini
[SparkFun Pro Micro]: https://www.sparkfun.com/products/12640
