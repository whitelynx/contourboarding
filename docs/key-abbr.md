Key names
=========

- `TAB`: Tab
- `CAP`: CapsLock
- `INS`: Insert
- `LS`: Left Shift
- `RS`: Right Shift
- `LAR`: Left Arrow
- `RAR`: Right Arrow
- `UAR`: Up Arrow
- `DAR`: Down Arrow
- `PS`: PrintScr/SysRq
- `SL`: Scroll lock
- `PB`: Pause/Break
- `KP`: Keypad
- `PG`: Progrm
- `LC`: Left Ctrl
- `RC`: Right Ctrl
- `LA`: Left Alt
- `RA`: Right Alt
- `HOM`: Home
- `END`: End
- `BS`: Back Space
- `DEL`: Delete
- `PGU`: Page Up
- `PGD`: Page Down
- `ENT`: Enter
- `SPC`: Space

- `PVT`: Scan Previous Track
- `PLP`: Play/Pause
- `STO`: Stop
- `NXT`: Scan Next Track
- `VU`: Keyboard Volume Up
- `VD`: Keyboard Volume Down
- `MUT`: Keyboard Mute
