Contourboarding
===============

A microcontroller project to create a new USB-capable core for Kinesis-brand contoured keyboards.

See also: [the Hackaday project](https://hackaday.io/project/161181-contourboarding)


History
-------

In December 2011, I started this project because I was fed up with having to use a PS/2-to-USB adapter for my keyboard
all the time. (as of this writing, I'm still using an adapter, though at least I found one that could handle the power
draw of this keyboard without losing connectivity every so often) I also had a couple of other ideas I wanted to
implement: Switchable keymaps that can be managed via USB (preferably treated as files exposed via
[USB mass storage][]), fast response times (sometimes I still end up getting stuck keys with this keyboard because of
how fast I type, and it's not very suitable for gaming as is), additional keys (like "media keys", etc.) and a built-in
TrackPoint.

The initial idea was to use a PIC18F4550 in order to implement a new USB-capable brain for the keyboard. After letting
the project languish for several years only making periodic attempts at poking it, I finally picked it up again. Since
I started, though, I've come to realize that PICs tend to require a bit too much legwork to get something working
compared to Atmel/Arduino-based boards. I'm now in the process of slowly revisiting my old code and designs, and
updating them to no longer require PICs.

I've also realized that I'd like to do a bit more with this board than I initially envisioned. I've had ideas of
cutting the board in half and allowing each half to be tilted and positioned independently, connecting them with a
cable. (likely SPI) I'd also like to integrate a USB hub, and if possible have the host port be a USB-C port. I'd also
like to keep the design extensible so I can come back and do more crazy things in the future. (maybe per-key RGB
lighting?)

[USB mass storage]: http://www.usb.org/developers/devclass_docs/usb_msc_cbi_1.1.pdf


Features
--------

- [ ] Multiple user-defined key maps that can be uploaded via USB and easily toggled between on the fly (I want this to replace my [Orbweaver][])
- [ ] A TrackPoint module (below N and to the left of the up arrow) and some thumb-operated mouse buttons (below Enter and Space)
- [ ] Possibly a scroll wheel or two (ideally a tilt-scroll wheel mounted to the left of H and N, or to the right of G and B; not sure which yet)
- [ ] Media control keys - both transport control (Play/Pause/Stop/Next/Prev) and volume control
- [ ] The ability to cut the keyboard in half and move the two pieces independently, with a tether between them
- [ ] Maybe even replace/augment the thumb buttons with something more akin to the [DataHand][]'s thumb configuration (modifiers on the sides?)

[Orbweaver]: https://www.razer.com/gaming-keyboards-keypads/razer-orbweaver-chroma
[DataHand]: http://octopup.org/computer/datahand


Getting Started
---------------

Right now the most useful entry points are probably the [Overall Hardware Design][] and [Overall Software Design][]
docs.

[Overall Hardware Design]: ./docs/overall-hardware-design.md
[Overall Software Design]: ./docs/overall-software-design.md


Project Layout
--------------

- `docs/`: Markdown files documenting various aspects of the project.
- `keymatrices/`: Text files representing various possible key matrix layouts.
- `source/`: Source code for the microcontroller.
- `tools/`: Various tools related to designing and implementing the project.
