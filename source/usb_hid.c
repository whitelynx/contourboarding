// Table 1: Usage Page Summary
// From HID Usage Tables 1.12
enum PageIDs
{
    HID_PAGE_UNDEFINED                     = 0x00,  // Undefined
    HID_PAGE_GENERIC_DESKTOP_CONTROLS      = 0x01,  // Generic Desktop Controls
    HID_PAGE_SIMULATION_CONTROLS           = 0x02,  // Simulation Controls
    HID_PAGE_VR_CONTROLS                   = 0x03,  // VR_Controls
    HID_PAGE_SPORT_CONTROLS                = 0x04,  // Sport Controls
    HID_PAGE_GAME_CONTROLS                 = 0x05,  // Game Controls
    HID_PAGE_GENERIC_DEVICE_CONTROLS       = 0x06,  // Generic Device Controls
    HID_PAGE_KEYBOARD_KEYPAD               = 0x07,  // Keyboard/Keypad
    HID_PAGE_LEDS                          = 0x08,  // LEDs
    HID_PAGE_BUTTON                        = 0x09,  // Button
    HID_PAGE_ORDINAL                       = 0x0A,  // Ordinal
    HID_PAGE_TELEPHONY                     = 0x0B,  // Telephony
    HID_PAGE_CONSUMER                      = 0x0C,  // Consumer
    HID_PAGE_DIGITIZER                     = 0x0D,  // Digitizer
    HID_PAGE_RESERVED                      = 0x0E,  // Reserved
    HID_PAGE_PID_PAGE                      = 0x0F,  // PID_Page
    HID_PAGE_UNICODE                       = 0x10,  // Unicode

    HID_PAGE_RESERVED_0_START              = 0x11,  // Reserved
    HID_PAGE_RESERVED_0_END                = 0x13,  // Reserved

    HID_PAGE_ALPHANUMERIC_DISPLAY          = 0x14,  // Alphanumeric Display

    HID_PAGE_RESERVED_1_START              = 0x15,  // Reserved
    HID_PAGE_RESERVED_1_END                = 0x3f,  // Reserved

    HID_PAGE_MEDICAL_INSTRUMENTS           = 0x40,  // Medical Instruments

    HID_PAGE_RESERVED_2_START              = 0x41,  // Reserved
    HID_PAGE_RESERVED_2_END                = 0x7F,  // Reserved

    HID_PAGE_MONITOR_PAGES_START           = 0x80,  // Monitor pages
    HID_PAGE_MONITOR_PAGES_END             = 0x83,  // Monitor pages
    HID_PAGE_POWER_PAGES_START             = 0x84,  // Power pages
    HID_PAGE_POWER_PAGES_END               = 0x87,  // Power pages

    HID_PAGE_RESERVED_3_START              = 0x88,  // Reserved
    HID_PAGE_RESERVED_3_END                = 0x8B,  // Reserved

    HID_PAGE_BAR_CODE_SCANNER_PAGE         = 0x8C,  // Bar Code Scanner page
    HID_PAGE_SCALE_PAGE                    = 0x8D,  // Scale page
    HID_PAGE_MAGNETIC_STRIPE_READERS       = 0x8E,  // Magnetic Stripe Reading (MSR) Devices
    HID_PAGE_RESERVED_POINT_OF_SALE_PAGES  = 0x8F,  // Reserved Point of Sale pages
    HID_PAGE_CAMERA_CONTROL_PAGE           = 0x90,  // Camera Control Page
}; // end PageIDs
