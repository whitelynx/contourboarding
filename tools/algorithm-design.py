"""This is just a Python testbed for some of the algorithms involved in processing the key matrix.

NOTE: This is NOT coded in a very Pythonic fashion; instead, I've tried to make the low-level code as much like C as
possible so that it's easier to ensure that the logic will be sound when ported to C.

ALSO NOTE: This is currently very PIC-specific. (even more specifically, this is built using the PIC18F4550's specs)

"""
from __future__ import print_function
from collections import OrderedDict
from os.path import dirname, join
import sys

from keymatrix import loadKeyMatrix, keyMatrixToUsagePageAndID, showMatrixAndStats
from usb_hid import HIDUsage


## Configuration ##

kmPath = join(dirname(dirname(sys.argv[0])), 'keymatrices')
matrixFilename = join(kmPath, 'contourboarding_20x5_v1.keymatrix')
if len(sys.argv) > 1:
    matrixFilename = sys.argv[1]

resultPorts = [
        # (Name, MaxBits)
        ('PORTB', 8),
        ('PORTD', 8),
        ('PORTA', 6),
        ]

# The number of simultaneous pressed keys to allow before sending phantom keypress messages.
pressedKeyBufSize = 8

## /Configuration ##


matrix = loadKeyMatrix(matrixFilename)
showMatrixAndStats(matrix, matrixFilename)
print()

pageIDMatrix = keyMatrixToUsagePageAndID(matrix)

showMatrixAndStats(pageIDMatrix, "With scancodes loaded", 6)
print()

portKeys = OrderedDict()

for queryLine, cols in enumerate(pageIDMatrix):
    curPortIdx = 0
    inputLinesMapped = 0
    while inputLinesMapped < len(cols):
        portName, bits = resultPorts[curPortIdx]
        portKeys.setdefault(portName, []).append(cols[inputLinesMapped:inputLinesMapped + bits])
        inputLinesMapped += bits
        curPortIdx += 1

for portName, keys in portKeys.items():
    showMatrixAndStats(keys, "Keys in " + portName, 6)
print()


# Example matrix state:
#  This is an example state for the 20x5 matrix layouts; it has 5 query (PIC->matrix) lines, each represented by an
# tuple of (PORTB[7:0], PORTD[7:0], PORTA[3:0]) in matrixState, and 20 result (matrix->PIC) lines, each represented by
# a given bit in one of the matrixState items.
matrixState = [
        # PORTB[7:0]  PORTD[7:0]  PORTA[3:0]
        #   __|___      __|___      _|
        #  /      \    /      \    /  \
        (0b00000000, 0b11000000, 0b0000),  # PORTE[2:0] = 0b000
        (0b00000000, 0b00000000, 0b0000),  # PORTE[2:0] = 0b001
        (0b00000000, 0b10000000, 0b0000),  # PORTE[2:0] = 0b010
        (0b00000000, 0b00000000, 0b0000),  # PORTE[2:0] = 0b011
        (0b00000000, 0b00000000, 0b0000),  # PORTE[2:0] = 0b100
        ]

showMatrixAndStats(
        [
            list(map(
                (lambda x: '1' if x == '1' else None),
                "".join([
                    bin(col)[2:].rjust([8, 8, 4][colIdx], '0')
                    for colIdx, col in enumerate(row)
                    ])
                ))
            for row in matrixState
            ],
        "Current keypresses",
        5
        )
print()

numQueryLines = len(matrixState)

# On the PIC, this would be RE[2:0] (or just RE, since it only has 3 accessible pins)
currentQuery = 0b000

# Or, len(matrxState)
maxQuery = 0b100


# PIC: Read from PORTB
def getPortB():
    return matrixState[currentQuery][0]


# PIC: Read from PORTD
def getPortD():
    return matrixState[currentQuery][1]


# PIC: Read from PORTA[3:0]
def getPortA():
    return matrixState[currentQuery][2]


TOO_MANY_KEYS_PRESSED = -1


def getPressedKeys():
    global currentQuery

    pressedKeys = [None] * pressedKeyBufSize
    pressedKeyCount = 0

    def getPressedKeysInPort(portKeys, state):
        # UGLY HACK: We can't modify pressedKeyCount directly here, because it would create a new pressedKeyCount in
        # getPressedKeysInPort's local scope instead, so we return the amount to increment by.  In C, it would just be
        # modifying the same location in memory.
        pressedKeyCountInc = 0

        bitsShifted = 0
        while state > 0:
            if state % 2 == 1:
                # Rightmost bit is set; log it as a key.
                if (pressedKeyCount + pressedKeyCountInc) > pressedKeyBufSize:
                    # More keys are pressed than we can track! Panic!
                    return TOO_MANY_KEYS_PRESSED

                pressedKeys[pressedKeyCount + pressedKeyCountInc] = keys[len(keys) - bitsShifted - 1]

                pressedKeyCountInc += 1

            state >>= 1
            bitsShifted += 1

        return pressedKeyCountInc

    currentQuery = 0
    while currentQuery < numQueryLines:
        # Hooray for copy/paste. -.- Did I ever mention I hate C?

        # PIC: Check PORTB
        state = getPortB()
        keys = portKeys['PORTB'][currentQuery]
        pressed = getPressedKeysInPort(keys, state)

        if pressed == TOO_MANY_KEYS_PRESSED:
            # We've pressed more keys than we can track, so we just send a phantom keypress message.
            print("Too many keys pressed! Giving up and sending a phantom message.")
            raise [None] * pressedKeyBufSize

        pressedKeyCount += pressed

        # PIC: Check PORTD
        state = getPortD()
        keys = portKeys['PORTD'][currentQuery]
        pressed = getPressedKeysInPort(keys, state)

        if pressed == TOO_MANY_KEYS_PRESSED:
            # We've pressed more keys than we can track, so we just send a phantom keypress message.
            print("Too many keys pressed! Giving up and sending a phantom message.")
            raise [None] * pressedKeyBufSize

        pressedKeyCount += pressed

        # PIC: Check PORTA
        state = getPortA()
        keys = portKeys['PORTA'][currentQuery]
        pressed = getPressedKeysInPort(keys, state)

        if pressed == TOO_MANY_KEYS_PRESSED:
            # We've pressed more keys than we can track, so we just send a phantom keypress message.
            print("Too many keys pressed! Giving up and sending a phantom message.")
            raise [None] * pressedKeyBufSize

        pressedKeyCount += pressed

        currentQuery += 1

    return pressedKeys[:pressedKeyCount]

print()
print("Pressed keys:")
print("\n".join(map(
        (lambda item: "  %s:%s (%s - %s)" % (item + (
            HIDUsage.pages_ValueToName[item[0]][item[1]],
            HIDUsage.pages_NameToDescription[item[0]][HIDUsage.pages_ValueToName[item[0]][item[1]]],
            ))),
        filter(None, getPressedKeys())
        )))
