#!/usr/bin/env python3
from __future__ import print_function
import io
import os
from os.path import abspath, dirname, isdir, isfile, islink, join, normpath
import string

try:
    # Python 3
    import html
    from http.server import HTTPServer, SimpleHTTPRequestHandler
    import urllib.parse as url

except ImportError:
    # Python 2
    import xml.sax.saxutils as html
    from BaseHTTPServer import HTTPServer
    from SimpleHTTPServer import SimpleHTTPRequestHandler
    import urllib as url

import pygments.formatters


md = None

try:
    import markdown
    md = markdown.Markdown(output_format='xhtml5', extensions=['codehilite', 'fenced_code', 'headerid'])

except ImportError:
    import markdown2
    md = markdown2.Markdown(extras=['code-friendly', 'code-color', 'fenced-code-blocks', 'header-ids', 'smarty-pants'])


rootDir = join(dirname(dirname(abspath(__file__))), 'docs')
rootDirComponents = rootDir.split(os.sep)


def getTemplate(tmplName):
    with open(join(rootDir, tmplName), 'r') as tmplFile:
        return string.Template(tmplFile.read())


pageTemplate = getTemplate('template.htm')


class RequestHandler(SimpleHTTPRequestHandler):
    def send_head(self):
        """Common code for GET and HEAD commands.

        This sends the response code and MIME headers.

        Return value is either a file object (which has to be copied
        to the outputfile by the caller unless the command was HEAD,
        and must be closed by the caller under all circumstances), or
        None, in which case the caller has nothing further to do.

        """
        path = self.translate_path(self.path)
        extraHeaders = {}

        if path is None:
            self.send_error(403, "Invalid path")
            return None

        elif isdir(path):
            if not self.path.endswith('/'):
                # Redirect browser - doing basically what Apache does
                self.send_response(301)
                self.send_header("Location", self.path + "/")
                self.end_headers()
                return None

            else:
                content = self.list_directory(path)

        elif isfile(path):
            fs = os.stat(path)
            extraHeaders = {"Last-Modified": self.date_time_string(fs.st_mtime)}

            if path.endswith('.md'):
                formatter = pygments.formatters.HtmlFormatter(style='manni')

                with open(path, 'r') as mdFile:
                    mdSource = mdFile.read()
                    if hasattr(mdSource, 'decode'):
                        mdSource = mdSource.decode('utf-8')

                    content = pageTemplate.safe_substitute(
                            title=self.path,
                            codehilite_css=formatter.get_style_defs('.codehilite'),
                            content=md.convert(mdSource)
                            )

            else:
                self.send_error(500, "Unhandled file type")
                return None

        else:
            self.send_error(404, "File not found")
            return None

        return self._respond(200, content, extraHeaders)

    def _respond(self, responseCode, content, otherHeaders={}):
        encoded = content.encode('utf-8')

        self.send_response(responseCode)

        self.send_header("Content-Type", "text/html; charset=utf-8")
        self.send_header("Content-Length", str(len(encoded)))

        for header, val in otherHeaders.items():
            self.send_header(header, val)

        self.end_headers()

        f = io.BytesIO()
        f.write(encoded)
        f.seek(0)
        return f

    def list_directory(self, path):
        """Helper to produce a directory listing.

        Return value is either a file object, or None (indicating an
        error).  In either case, the headers are sent, making the
        interface the same as for send_head().

        """
        try:
            dirList = os.listdir(path)
        except os.error:
            self.send_error(404, "No permission to list directory")
            return None

        dirList.sort(key=lambda a: a.lower())

        displaypath = html.escape(url.unquote(self.path))
        title = 'Directory listing for {}'.format(displaypath)

        r = ['<h1>{}</h1>'.format(html.escape(title))]

        for name in dirList:
            fullname = join(path, name)

            if isfile(fullname) and not name.endswith('.md'):
                continue

            displayname = linkname = name

            # Append / for directories or @ for symbolic links
            if isdir(fullname):
                displayname = name + "/"
                linkname = name + "/"

            if islink(fullname):
                displayname = name + "@"
                # Note: a link to a directory displays with @ and links with /

            r.append('<li><a href="{}">{}</a></li>'.format(url.quote(linkname), html.escape(displayname)))

        r.append('</ul>')

        content = pageTemplate.safe_substitute(
                title=title,
                codehilite_css='',
                content='\n'.join(r)
                )

        return content

    def translate_path(self, path):
        """Translate a /-separated PATH to the local filename syntax.

        Translates the path directly to a filesystem path relative to `rootDir`; if the resulting path is inside
        `rootDir`, the resulting path is returned, otherwise `None` is returned.

        """
        # Remove query parameters and fragment.
        path = path.split('?', 1)[0]
        path = path.split('#', 1)[0]

        # Remove leading slashes.
        path = path.lstrip('/')

        path = normpath(join(rootDir, url.unquote(path)))

        components = path.split(os.sep)
        if components[:len(rootDirComponents)] == rootDirComponents:
            # Resulting path is inside rootDir; return it.
            return path

        # Resulting path is outside rootDir! Return None.
        return None


server_address = ('', 8000)
httpd = HTTPServer(server_address, RequestHandler)

print("Serving docs from {} on {}:{}...".format(rootDir, *server_address))
httpd.serve_forever()
