# -*- coding: utf-8 -*-
"""Calculate possible key matrix configurations for a given number of keys.

This calculates and displays parameters which pertain to one of the following general circuits:

When "Use Demultiplexer" is "no":

    ╔═══╗ ╔═══╗ ╔═══╗
    ║key╟─╢key╟─╢key╟──────┐
    ╚═╤═╝ ╚═╤═╝ ╚═╤═╝      │
      │     │     │  ...   │
    ╔═╧═╗ ╔═╧═╗ ╔═╧═╗      │ ...
    ║key╟─╢key╟─╢key╟────┐ └──────────────────┐
    ╚═╤═╝ ╚═╤═╝ ╚═╤═╝    │                    │
      │     │     │  ... └────────────────┐   │
    ╔═╧═╗ ╔═╧═╗ ╔═╧═╗                     │   │
    ║key╟─╢key╟─╢key╟─────────────────┐   │   │
    ╚═╤═╝ ╚═╤═╝ ╚═╤═╝         ▴       │   │   │
      │ ... │ ... │           │       │   │   │
      │     └─┐   │      Query Lines  │   │   │
      └───┐   │   │                   │   │   │
          │   │   │                   │   │   │
          │   │   │                   │   │   │
       ...│   │   │ ◂─ Response Lines │   │   │
          │   │   │                   │   │   │
          │   │   │                   │   │   │
          │   │   │                   │   │   │
          │   │   │                   │   │   │
          │   │   │                   │   │   │
        ┌─┴───┴───┴───────────────────┴───┴───┴─┐
        │ 0   1   ...   ◂─ Pins ─▸   ... n-1  n │
        │            Microcontroller            │


When "Use Demultiplexer" is "yes":

    ╔═══╗ ╔═══╗ ╔═══╗
    ║key╟─╢key╟─╢key╟──────┐
    ╚═╤═╝ ╚═╤═╝ ╚═╤═╝      │
      │     │     │  ...   │
    ╔═╧═╗ ╔═╧═╗ ╔═╧═╗      │ ...
    ║key╟─╢key╟─╢key╟────┐ └─────────────────────────────────┐
    ╚═╤═╝ ╚═╤═╝ ╚═╤═╝    │                                   │
      │     │     │  ... └────────────────────────────────┐  │
    ╔═╧═╗ ╔═╧═╗ ╔═╧═╗                                     │  │
    ║key╟─╢key╟─╢key╟──────────────────────────────────┐  │  │
    ╚═╤═╝ ╚═╤═╝ ╚═╤═╝         ▴                        │  │  │
      │ ... │ ... │           │                        │  │  │
      │     └─┐   │      Query Lines   Query Selection │  │  │...
      └───┐   │   │                          │         │  │  │
          │   │   │                          ▾      ╱──┴──┴──┴────╲
          │   │   │                   ┌────────────╱               ╲
       ...│   │   │ ◂─ Response Lines │   ┌───────╱  Demultiplexer  ╲
          │   │   │                   │   │ ...  ╱────────┬──────────╲
          │   │   │                   │   │               │
          │   │   │                   │   │               │ ◂─ Query Activation
          │   │   │                   │   │   ┌───────────┘
          │   │   │                   │   │   │
        ┌─┴───┴───┴───────────────────┴───┴───┴─┐
        │ 0   1   ...   ◂─ Pins ─▸   ... n-1  n │
        │            Microcontroller            │

"""
from __future__ import print_function
import math
import string


matrixConfigTips = """\033[1;4mTips For Choosing A Matrix Configuration:\033[m
The \"flatter\" the matrix, the better. Every row in the matrix (first column above) you add will
cost a given amount of time, and the sum of the times for all query lines is the limiting factor
in the speed at which the keyboard can detect keystrokes. If this gets too slow, the keyboard will
detect key presses out of order, or even completely miss key presses, if you type too fast. See the
\033[1mResponse Time\033[m section below for more information.

Make sure you have enough microcontroller pins available for the total pin count of your chosen
matrix configuration. (the bold number) Keep in mind that most microcontrollers require at least
the following pins, which won't be available for use with the keyboard matrix:

 - 2 to 4 pins for power (at least ground and +5V; sometimes two of each)
 - 2 to 6 pins for the data bus (2 pins for PS/2, 3 pins for USB on a PIC18F4550 with the internal
      USB transceiver, 6 pins when using an external transceiver)
 - pins for LEDs, communication with other chips, etc.

\033[1;4mResponse Time:\033[m
Your keyboard's minimum response time can be calculated with the following equation:

    minRTime = queryLines * (hardwareLatency + softwareLatencyPerRow) + softwareLatencyPerPass

Each of the components in this equation should be minimized as much as possible, but the per-row
latency (the part inside the parenthesis) and the number of query lines together have the biggest
impact on the overall response time, and should be carefully optimized

  \033[4mHardware Latency:\033[m
  The hardware latency is the sum of the response times or propagation delays of every component
  that is part of the key matrix circuit. This includes the max. propagation delay of each
  microcontroller output pin and the max. response time of each microcontroller input pin, as well
  as propagation delays for any diodes or demultiplexers used. See the datasheet for each component
  to calculate this.

  \033[4mSoftware Latency:\033[m
  The software latency is divided into 2 components: time per row, and time per pass. The time
  per row should be relatively low, as all your software should need to do is set up the output
  pins (query lines) before each row, and read the input pins after the hardware latency time has
  elapsed. However, be careful to optimize this code, since can have a bigger effect on the overall
  response time than any other part of the software.

  The per-pass latency is likely to me more substantial, but also contributes less to the overall
  response time, since it is not multiplied by the number of query lines. Generally, this code will
  be responsible for sending any detected keypresses over whatever data bus the keyboard uses.

  One possible technique for minimizing software latency is to, after setting up the outputs for
  a given row, use a timer interrupt instead of a busy loop to wait for the hardware latency time
  to pass, so that you can continue processing while the hardware responds.
"""


def demuxSelectLines(demuxOutputs):
    """Calculate the minimum number of demultiplexer select lines needed to obtain the given number of output lines.

    According to https://secure.wikimedia.org/wikipedia/en/wiki/Multiplexer:

        "A multiplexer of 2^n inputs has n select lines"

    Since demultiplexers are the inverse (requiring n select lines for 2^n _outputs_), this works out to:

        For a demultiplexer of at least n inputs, ceil(log_2(n)) select lines are required.

    Note that the demultiplexer's _outputs_ are the _query lines_.

    """
    return int(math.ceil(math.log(demuxOutputs, 2)))


def useDemux(queryLines):
    """Only use a demultiplexer if it will actually save us pins.

    The total number of pins used will be the number of demultiplexer select lines, plus demultiplexer input line.

    Note that the demultiplexer's _outputs_ are the _query lines_.

    """
    return demuxSelectLines(queryLines) + 1 < queryLines


matrixBaseTemplate = "  ${queryLines} x ${responseLines} = ${totalKeys} (${extraKeys} extra)   " \
        + "\033[1m${mcTotalPins} total pins\033[m (${mcOutputPins} output, "

matrixWithDemuxTemplate = string.Template(
        matrixBaseTemplate
        + "${mcSelectPins} select, ${mcInputPins} input; "
                "using ${usedDemuxOutputLines} outputs of a 1-to-${demuxOutputLines} demultiplexer)"
        )

matrixNoDemuxTemplate = string.Template(
        matrixBaseTemplate + "${mcInputPins} input)"
        )


def calculateMatrixOptions(numKeys):
    """Calculate and display possible key matrix configurations that could accomodate the given number of keys.

    We only worry about the minimum-sized configuration for each number of inputs, since we're trying to conserve pins.

    """
    for queryLines in range(2, int(math.ceil(math.sqrt(numKeys)))):
        responseLines = int(math.ceil(float(numKeys) / queryLines))
        totalKeys = queryLines * responseLines

        if useDemux(queryLines):
            mcOutputPins = 1
            mcSelectPins = demuxSelectLines(queryLines)
            demuxOutputLines = 2 ** mcSelectPins
            usedDemuxOutputLines = queryLines
            template = matrixWithDemuxTemplate

        else:
            mcOutputPins = queryLines
            mcSelectPins = 0
            demuxOutputLines = 0
            usedDemuxOutputLines = 0
            template = matrixNoDemuxTemplate

        print(template.substitute(
                queryLines=queryLines,
                responseLines=responseLines,
                mcTotalPins=mcOutputPins + mcSelectPins + responseLines,
                mcOutputPins=mcOutputPins,
                mcSelectPins=mcSelectPins,
                mcInputPins=responseLines,
                demuxOutputLines=demuxOutputLines,
                usedDemuxOutputLines=usedDemuxOutputLines,
                totalKeys=totalKeys,
                extraKeys=totalKeys - numKeys
                ))


if __name__ == '__main__':
    import sys

    def usage():
        print("Usage:", file=sys.stderr)
        print("   ", sys.argv[0], "NUM_KEYS", file=sys.stderr)
        sys.exit(1)

    if len(sys.argv) < 2:
        usage()

    for numKeys in map(int, sys.argv[1:]):
        print("\033[1;4mPossible keyboard matrix configurations to accomodate", numKeys, "keys:\033[m")
        calculateMatrixOptions(numKeys)
        print()

    print(matrixConfigTips)
