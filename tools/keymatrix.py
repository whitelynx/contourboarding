from __future__ import print_function

from usb_hid import HIDUsage


def keyMatrixToLists(matrix_src, columnSep='\t', lineSep='\n', commentChar='#'):
    """Converts a text representation of a key matrix to a list of lists.

    `matrix_src` should be a string which consists of lines (separated by `lineSep`; defaults to `\\n`) of one of the
    following formats:
    - A row of keys: a line number (`/[^$columnSep]*/`, default `/[^\\t]*/`), followed by `columnSep` (defaults `\\t`),
        followed by a list of key names (`/[^$columnSep]*/`) separated by `columnSep`
    - A comment: `commentChar` (defaults to `#`) followed by a comment (`/.*/`)

    """
    keyMatrixList = list()
    longestRow = 0

    for line in matrix_src.split(lineSep):
        if line.startswith('#') or len(line) == 0:
            # Skip comment lines
            continue

        # Remove the first column (row number) and replace empty strings with None.
        columns = [col or None for col in line.split(columnSep)[1:]]

        # Track the length of the longest row.
        longestRow = max(longestRow, len(columns))

        keyMatrixList.append(columns)

    # Ensure all rows are the same length.
    for row in keyMatrixList:
        if len(row) < longestRow:
            row.extend([None] * (longestRow - len(row)))

    return keyMatrixList


def countUsedCells(matrix):
    return sum(map(lambda row: len(list(filter(lambda x: x is not None, row))), matrix))


def countUnusedCells(matrix):
    return sum(map(lambda row: len(list(filter(lambda x: x is None, row))), matrix))


def displayCell(cell, colWidth=5):
    if cell is None:
        return '%s'.ljust(colWidth - 2) % (
                '\033[1;30mNone\033[m',
                )
    elif isinstance(cell, tuple) and len(cell) == 2:
        # Assume it's a (pageID, usageID) tuple, and print in a more compact way.
        return "{}:{}".format(*cell).ljust(colWidth)
    else:
        return str(cell).ljust(colWidth)


def displayMatrix(matrix, indent="", colWidth=5):
    print(indent + "   "
            + "".join("\033[34m{}\033[m".format(str(colIdx).ljust(colWidth))
                for colIdx in range(len(matrix[0])))
            + "\n" + indent
            + ("\n" + indent).join(
                "\033[34m{: >2}\033[m ".format(rowIdx)
                + "".join(map(lambda c: displayCell(c, colWidth), row))
                for rowIdx, row in enumerate(matrix)
                ))


def showMatrixAndStats(matrix, name, colWidth=5):
    if not isinstance(matrix, (list, tuple)):
        matrix = list(matrix)

    print("{}: ({} by {})".format(name, len(matrix[0]), len(matrix)))
    print("   {} used cells".format(countUsedCells(matrix)))
    print("   {} unused cells".format(countUnusedCells(matrix)))

    print("   Matrix:")
    displayMatrix(matrix, "      ", colWidth)


def loadKeyMatrix(filename):
    src = open(filename, 'r').read()
    return keyMatrixToLists(src)


def keyMatrixToUsagePageAndID(matrix):
    return [
            [
                HIDUsage.getPageAndID(keysym)
                for keysym in row
                ]
            for row in matrix
            ]


if __name__ == '__main__':
    import sys
    from os.path import dirname, join

    kmPath = join(dirname(dirname(sys.argv[0])), 'keymatrices')

    kinesis_key_matrix_src = open(join(kmPath, 'original-kinesis.keymatrix'), 'r').read()
    humblehacker_key_matrix_src = open(join(kmPath, 'humblehacker.keymatrix'), 'r').read()
    contourboarding_old_key_matrix_src = open(join(kmPath, 'contourboarding_old.keymatrix'), 'r').read()

    kinesis_key_matrix = keyMatrixToLists(kinesis_key_matrix_src)
    humblehacker_key_matrix = keyMatrixToLists(humblehacker_key_matrix_src)
    contourboarding_old_key_matrix = keyMatrixToLists(contourboarding_old_key_matrix_src)

    showMatrixAndStats(kinesis_key_matrix, "Original Kinesis key matrix")

    showMatrixAndStats(humblehacker_key_matrix, "Humblehacker key matrix")

    showMatrixAndStats(contourboarding_old_key_matrix, "Contourboarding key matrix")
